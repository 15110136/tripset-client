import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";
import { connect } from "react-redux";

import Main from "./containers/Main";
import Login from "./components/Login";

import Header from "./containers/Header";
import Footer from "./components/Footer";

import { testActions } from "./store/actions";

import logo from "./logo.svg";
import "./styles/global.sass";

class App extends Component {
  componentDidMount() {
    this.props.testActions();
  }

  render() {
    return (
      <React.Fragment>
        <Header />
        <Switch>
          <Route exact path="/" component={Main} />
          <Route path="/login" component={Login} />
        </Switch>
        <Footer />
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
  testActions: () => dispatch(testActions())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
